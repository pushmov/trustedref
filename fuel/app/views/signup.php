<form class="form-basic" method="post" action="#">
	<div class="row">
		<div class="small-12 medium-6 columns">
			<div class="clearfix">
				<fieldset>
					<h4>My Details</h4>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">First Name <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Last Name <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Phone <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Website <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>
				</fieldset>
			</div>
			
			<div class="clearfix">
				<fieldset>
					<h4>Login Details</h4>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Email <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Password <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Password Again <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>

				</fieldset>
			</div>
			
			
			<div class="row">
				<div class="small-12 columns">
					<button class="button alert-success">Signup</button>
					<button class="button alert-danger">Cancel</button>
				</div>
			</div>
			
		</div>
		<div class="small-12 medium-6 columns">
			<div class="clearfix">
				<fieldset>
					<h4>My Team</h4>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Team Name <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Single Name <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Slogan <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Email <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Website <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>
				</fieldset>
			</div>
			
			<div class="clearfix">
				<fieldset>
					<h4>Broker Details</h4>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Board ID <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Name <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Address <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">City <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">State <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">ZIP <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Country <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Phone <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="right-label" class="right inline">Website <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="name">
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
</form>