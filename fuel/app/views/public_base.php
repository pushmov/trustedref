<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?=$meta_title;?></title>
		
		<link href="/assets/css/foundation.min.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="/assets/css/style.css" rel="stylesheet" type="text/css" media="screen" />

		
		<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
		<script src="/assets/js/foundation.min.js" type="text/javascript"></script>
	</head>
	<body>
		<header>
			<h1>header</h1>
		</header>
		
		<div class="main-content">
			<?=$content;?>
		</div>
		<footer>
			<h1>footer</h1>
		</footer>
	</body>
</html>