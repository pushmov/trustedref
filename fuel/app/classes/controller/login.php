<?php

namespace Controller;

class Login extends PublicTemplate {
	public function before() {
		parent::before();
	}
	
	public function action_index(){
		$content = \View::forge('login');
		$this->template->meta_title = 'Login';
		$this->template->content = $content;
	}
}