<?php
namespace Controller;

class PublicTemplate extends \Controller_Hybrid {
	public $template = 'public_base';
	public $member = null;

	public function before() {
		parent::before();
	}
}