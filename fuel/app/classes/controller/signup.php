<?php

namespace Controller;

class Signup extends PublicTemplate {
	
	public function before() {
		parent::before();
	}
	
	public function action_index(){
		$content = \View::forge('signup');
		$this->template->meta_title = 'Registration';
		$this->template->content = $content;
	}
	
}