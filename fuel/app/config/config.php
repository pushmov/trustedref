<?php
/**
 * Part of the Fuel framework.
 *
 * @package    Fuel
 * @version    1.7
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2015 Fuel Development Team
 * @link       http://fuelphp.com
 */

 define('DATE_FORMAT','F j, Y');
 define('DATETIME_FORMAT','F j, Y h:i:s A');
 define('DATETIME_FORMAT_DB', 'Y-m-d H:i:s');

return array(
	'locale'             => '',
	'security' => array(
		'uri_filter'       => array('htmlentities'),
		'output_filter'  => array(),
		'whitelisted_classes' => array(
			'Fuel\\Core\\Presenter',
			'Fuel\\Core\\Response',
			'Fuel\\Core\\View',
			'Fuel\\Core\\ViewModel',
			'Closure',
		),
	),
	'controller_prefix' => 'Controller\\',
	'package_paths' => array(
		PKGPATH,
	),
	'always_load'  => array(
		'packages'  => array(
			'authlite',
			'automodeler',
		),
		'classes'  => array(
			'session',
		),
		'config'  => array(
			'session',
		),
	 ),
	 'email_bcc' => array(
	 ),

);
